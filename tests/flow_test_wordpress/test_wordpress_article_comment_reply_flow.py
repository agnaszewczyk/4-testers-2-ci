import time
from base64 import b64encode
import requests
import pytest
import lorem

## Entry data ##

editor_username = 'editor'
editor_password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
editor_email = 'editor@somesite.com'
editor_token = b64encode(f"{editor_username}:{editor_password}".encode('utf-8')).decode("ascii")

commenter_username = 'commenter'
commenter_password = 'SXlx hpon SR7k issV W2in zdTb'
commenter_email = 'commenter@somesite.com'
commenter_token = b64encode(f"{commenter_username}:{commenter_password}".encode('utf-8')).decode("ascii")

blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post ASz" + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph()
    }
    return article


@pytest.fixture(scope='module')
def headers():
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Basic " + editor_token
    }
    return headers


@pytest.fixture(scope='module')
def commenter_headers():
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Basic " + commenter_token
    }
    return headers


@pytest.fixture(scope='module')
def posted_article(article, headers):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish"
    }
    response = requests.post(url=posts_endpoint_url, headers=headers, json=payload)
    return response


## Step 1 - Add new article as author and check, that the author has created the post/article which can be read ##
def test_editor_successfully_created_the_post(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"

def test_new_post_from_editor_can_be_read(article, posted_article):
        wordpress_post_id = posted_article.json()['id']
        wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
        published_article = requests.get(url=wordpress_post_url)
        assert published_article.status_code == 200
        assert published_article.reason == "OK"
        wordpress_post_data = published_article.json()
        print(wordpress_post_data)
        author_link = wordpress_post_data['_links']['author'][0]['href']
        print(author_link)
        assert wordpress_post_data['_links']['author'][0]['href'] == author_link
        assert wordpress_post_data["title"]["rendered"] == article["article_title"]
        assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{article["article_subtitle"]}</p>\n'
        assert wordpress_post_data["content"]["rendered"] == f'<p>{article["article_text"]}</p>\n'
        assert wordpress_post_data["status"] == 'publish'

    ## Step 2 - add comment as commenter to the newly created post/article ##
def test_commenter_successfully_created_the_comment(posted_article, commenter_headers):

    # Input data for the comment
    comment_data = {
        "post": posted_article.json()['id'],
        "content": "Short comment on the posted article"
    }

    # 'POST' to create new comment
    response = requests.post(f"{blog_url}/wp-json/wp/v2/comments", json=comment_data, headers=commenter_headers)

    # Check, if the comment has been created successfully
    assert response.status_code == 201
    assert response.reason == "Created"

    # Check, if comment is connected with the right post/article
    comment = response.json()
    assert comment["post"] == posted_article.json()['id']

    # Check, that the commenter has created the comment
    author_link = comment['author_name']
    print(author_link)
    assert comment['author_name'] == author_link


## Step 3 - Editor replies to the comment (from step 2) under the article (from step 1) ##
def test_reply_created_by_the_editor(posted_article, headers):
    # Get ID of the comment under the post/article
    comments_url = f"{blog_url}/wp-json/wp/v2/comments?post={posted_article.json()['id']}"
    comments_response = requests.get(comments_url)
    comment_id = comments_response.json()[0]['id']

    # Input data for the reply
    reply_data = {
        "post": posted_article.json()['id'],
        "parent": comment_id,
        "content": "Punch reply from the editor"
    }

    # 'POST' to create new reply
    response = requests.post(f"{blog_url}/wp-json/wp/v2/comments", json=reply_data, headers=headers)

    # Check, if the reply has been created successfully
    assert response.status_code == 201
    assert response.reason == "Created"

    # Check, if reply is connected with the right post and comment
    reply = response.json()
    assert reply["post"] == posted_article.json()["id"]
    assert reply["parent"] == comment_id

    # Check, that the author has created the reply
    author_response = requests.get(reply["_links"]["author"][0]["href"])
    author_data = author_response.json()
    assert author_data["name"] == 'John Editor'
